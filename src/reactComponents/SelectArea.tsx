/**
 * Created by glenn on 6/12/2017.
 */

import React = require("react");
import LayerBaseVectorGeoJson from '../layers/LayerBaseVectorGeoJson';
import {proj3857, proj4326} from '../olHelpers/projections'
import makeGuid from '../util/makeGuid';
import getMap from './helpers/get_map';
import Map from 'ol/Map'
import Draw from 'ol/interaction/Draw';
import Style from 'ol/style/Style';
import Stroke from 'ol/style/Stroke';
import Fill from 'ol/style/Fill';
import Polygon from 'ol/geom/Polygon';
import Feature from 'ol/Feature';
import GeometryType from 'ol/geom/GeometryType';

export interface iSelectArea{
    map: Map | (() => Map);
    callback: (coords: Array<number[]>) => any
}

export class SelectArea extends React.Component<iSelectArea, {}> {
    map: Map;
    callback: (coords: Array<number[]>) => any;
    areaOverlay: LayerBaseVectorGeoJson;
    draw: Draw;
    selectId: string;
    cancelId: string;
    selectButton: HTMLButtonElement;
    cancelButton: HTMLButtonElement;


    constructor(props: iSelectArea, context: Object) {
        super(props, context);

        this.selectId = makeGuid();
        this.cancelId = makeGuid();

        this.callback = this.props.callback;

        this.areaOverlay = new LayerBaseVectorGeoJson('',
            {
                style: new Style({
                    fill: new Fill({
                        color: 'rgba(255, 0, 237, 0.1)'
                    }),
                    stroke: new Stroke({
                        color: 'rgb(255, 0, 237)',
                        width: 2
                    })
                }),
                transform: {dataProjection: proj4326, featureProjection: proj3857}
            });

        this.draw = new Draw({
            source: this.areaOverlay.source,
            type: GeometryType.POLYGON
        });

        // this.draw.on('drawend', (evt: {feature: {getGeometry: () => Polygon}}) => {
        this.draw.on('drawend', (evt) => {
            this.selectButton.style.display = '';
            this.cancelButton.style.display = 'none';



            let geom = (evt['feature'] as Feature).getGeometry() as Polygon;
            let geomClone = geom.clone() as Polygon;

            geomClone.transform('EPSG:3857', 'EPSG:4326');

            setTimeout(() => {
                this.map.removeInteraction(this.draw);
            }, 100);

            let outCoords = [];
            let ccc = (geomClone as Polygon).getCoordinates()[0];

            for (let cc of ccc) {
                outCoords.push([Math.round(cc[0] * 1000000) / 1000000, Math.round(cc[1] * 1000000) / 1000000]);
            }

            this.callback(outCoords);
        });
    }


    componentDidMount() {
        this.selectButton = document.getElementById(this.selectId) as HTMLButtonElement;
        this.cancelButton = document.getElementById(this.cancelId) as HTMLButtonElement;
        getMap(this.props.map, this.areaOverlay.olLayer).then((m) => {this.map = m})
    }


    setArea() {
        if (!this.map) {
            return;
        }

        this.selectButton.style.display = 'none';
        this.cancelButton.style.display = '';

        this.areaOverlay.source.clear();
        this.map.addInteraction(this.draw);
        this.callback(null);
    }

    cancel() {
        if (!this.map) {
            return;
        }
        this.selectButton.style.display = '';
        this.cancelButton.style.display = 'none';

        this.areaOverlay.source.clear();
        this.map.removeInteraction(this.draw);

        this.callback(null);
    }

    render() {
        return <div className="ol-select-area" style={{margin: '10px'}}>
            <button id={this.selectId} onClick={() => {
                this.setArea()
            }}>Select Area
            </button>
            <button id={this.cancelId} onClick={() => {
                this.cancel()
            }} style={{display: 'none'}}>Cancel
            </button>
        </div>
    }
}
