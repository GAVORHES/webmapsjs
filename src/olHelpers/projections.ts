/**
 * Created by gavorhes on 10/3/2016.
 */

import Projection from 'ol/proj/Projection';


export const proj4326 = new Projection({code: 'EPSG:4326'});
export const proj3857 = new Projection({code: 'EPSG:3857'});
export const proj3070 = new Projection({code: 'EPSG:3070'});

