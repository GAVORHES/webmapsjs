"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by gavorhes on 12/4/2015.
 */
const LayerBase_1 = require("./LayerBase");
const provide_1 = require("../util/provide");
const nm = provide_1.default('layers');
const XYZ_1 = require("ol/source/XYZ");
const Tile_1 = require("ol/layer/Tile");
/**
 * XYZ tile
 * @augments LayerBase
 */
class LayerBaseXyzTile extends LayerBase_1.LayerBase {
    /**
     * The XYZ tile layer
     * @param {string} url - url for source
     * @param {object} options - config
     * @param {string} [options.id] - layer id
     * @param {string} [options.name=Unnamed Layer] - layer name
     * @param {number} [options.opacity=1] - opacity
     * @param {boolean} [options.visible=true] - default visible
     * @param {number} [options.minZoom=undefined] - min zoom level, 0 - 28
     * @param {number} [options.maxZoom=undefined] - max zoom level, 0 - 28
     * @param {object} [options.params={}] the get parameters to include to retrieve the layer
     * @param {number} [options.zIndex=0] the z index for the layer
     * @param {function} [options.loadCallback] function to call on load, context this is the layer object
     * @param {boolean} [options.legendCollapse=false] if the legend item should be initially collapsed
     * @param {boolean} [options.legendCheckbox=true] if the legend item should have a checkbox for visibility
     * @param {boolean} [options.legendContent] additional content to add to the legend
     * @param {boolean} [options.useEsriStyle=false] if the map service style should be used
     */
    constructor(url, options = {}) {
        super(url, options);
        this._source = new XYZ_1.default({ url: this.url == '' ? undefined : this.url });
        this._olLayer = new Tile_1.default({
            source: this._source,
            visible: this.visible,
            opacity: this.opacity,
            minResolution: this._minResolution,
            maxResolution: this._maxResolution
        });
        this._olLayer.setZIndex(this._zIndex);
    }
    get source() {
        return this._source;
    }
    get olLayer() {
        return this._olLayer;
    }
}
exports.LayerBaseXyzTile = LayerBaseXyzTile;
nm.LayerBaseXyzTile = LayerBaseXyzTile;
exports.default = LayerBaseXyzTile;
//# sourceMappingURL=LayerBaseXyzTile.js.map