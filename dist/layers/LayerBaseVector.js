"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const LayerBase_1 = require("./LayerBase");
const mapMove_1 = require("../olHelpers/mapMove");
const provide_1 = require("../util/provide");
const $ = require("jquery");
const Vector_1 = require("ol/layer/Vector");
const Vector_2 = require("ol/source/Vector");
const Projection_1 = require("ol/proj/Projection");
const nm = provide_1.default('layers');
/**
 * The Vector layer base
 * @augments LayerBase
 * @abstract
 */
class LayerBaseVector extends LayerBase_1.LayerBase {
    /**
     * The base vector layer
     * @param {string} url - pass an empty string to prevent default load and add from a json source
     * @param {object} options - config
     * @param {string} [options.id] - layer id
     * @param {string} [options.name=Unnamed Layer] - layer name
     * @param {number} [options.opacity=1] - opacity
     * @param {boolean} [options.visible=true] - default visible
     * @param {number} [options.minZoom=undefined] - min zoom level, 0 - 28
     * @param {number} [options.maxZoom=undefined] - max zoom level, 0 - 28
     * @param {object} [options.params={}] the get parameters to include to retrieve the layer
     * @param {number} [options.zIndex=0] the z index for the layer
     * @param {function} [options.loadCallback] function to call on load, context this is the layer object
     * @param {boolean} [options.legendCollapse=false] if the legend item should be initially collapsed
     * @param {boolean} [options.legendCheckbox=true] if the legend item should have a checkbox for visibility
     * @param {boolean} [options.legendContent] additional content to add to the legend
     *
     * @param {boolean} [options.autoLoad=false] if the layer should auto load if not visible
     * @param {object} [options.style=undefined] the layer style, use openlayers default style if not defined
     * @param {boolean} [options.onDemand=false] if the layer should be loaded by extent on map move
     * @param {number} [options.onDemandDelay=300] delay before the map move callback should be called
     * @param {mapMoveMakeGetParams} [options.mapMoveMakeGetParams=function(lyr, extent, zoomLevel){}] function to create additional map move params
     * @param {MapMoveCls} [options.mapMoveObj=mapMove] alternate map move object for use with multi map pages
     *
     */
    constructor(url, options = {}) {
        super(url, options);
        options = options;
        //prevent regular load if no url has been provided
        if (this.url.trim() == '') {
            this._loaded = true;
        }
        this._style = typeof options.style == 'undefined' ? undefined : options.style;
        if (this.visible) {
            this._autoLoad = true;
        }
        else {
            this._autoLoad = (typeof options['autoLoad'] == 'boolean' ? options['autoLoad'] : false);
        }
        this._onDemand = typeof options.onDemand == 'boolean' ? options.onDemand : false;
        this._onDemandDelay = typeof options.onDemandDelay == 'number' ? options.onDemandDelay : 300;
        if (options.mapMoveObj) {
            this._mapMove = options.mapMoveObj;
        }
        else {
            this._mapMove = this._onDemand ? mapMove_1.default : undefined;
        }
        this._mapMoveMakeGetParams = typeof options.mapMoveMakeGetParams == 'function' ? options.mapMoveMakeGetParams :
            function () { return {}; };
        if (this._onDemand) {
            this._loaded = true;
            this._mapMoveParams = {};
            this._mapMove.checkInit();
            this._mapMove.addVectorLayer(this);
        }
        this._source = new Vector_2.default();
        this._olLayer = new Vector_1.default({
            source: this._source,
            visible: this.visible,
            style: this.style,
            minResolution: this._minResolution,
            maxResolution: this._maxResolution,
            renderOrder: options.renderOrder
        });
        this.olLayer.setZIndex(this._zIndex);
        this._projectionMap = null;
        this._projection4326 = new Projection_1.default({ code: "EPSG:4326" });
        this._olLayer.setOpacity(this.opacity);
    }
    /**
     * dummy to be overridden
     * @param {object} featureCollection - geojson or esrijson object
     */
    addFeatures(featureCollection) {
        console.log('Layer vector base addFeatures is a placeholder and does nothing');
    }
    /**
     * Before call to map move callback, can prevent call by returning false
     * @param {number} zoom - zoom level
     * @param {string} [evtType=undefined] undefined for initial load, otherwise one of 'change:center', 'change:resolution'
     * @returns {boolean} if the call should proceed
     */
    mapMoveBefore(zoom, evtType) {
        if (this.minZoom !== undefined) {
            if (zoom < this.minZoom) {
                return false;
            }
        }
        if (this.maxZoom !== undefined) {
            if (zoom > this.maxZoom) {
                return false;
            }
        }
        return this.visible;
    }
    /**
     * callback to generate the parameters passed in the get request
     * @param {object} extent - extent object
     * @param {number} extent.minX - minX
     * @param {number} extent.minY - minY
     * @param {number} extent.maxX - maxX
     * @param {number} extent.maxY - maxY
     * @param {number} zoomLevel - zoom level
     */
    mapMoveMakeGetParams(extent, zoomLevel) {
        this._mapMoveParams = {};
        $.extend(this._mapMoveParams, this.params);
        $.extend(this._mapMoveParams, this._mapMoveMakeGetParams(this, extent, zoomLevel));
    }
    /**
     * callback function on map move
     * @param {object} d - the json response
     */
    mapMoveCallback(d) {
        if (this.source) {
            this._source.clear();
        }
    }
    /**
     * clear features in the layer
     */
    clear() {
        if (this._source) {
            this._source.clear();
        }
    }
    /**
     * get on demand delay in miliseconds
     */
    get onDemandDelay() {
        return this._onDemandDelay;
    }
    /**
     * get if the layer is autoloaded
     */
    get autoLoad() {
        return this._autoLoad;
    }
    /**
     * get the style definition
     */
    get style() {
        return this._style;
    }
    /**
     * set the style
     * @param style - the style or function
     */
    set style(style) {
        this._style = style;
        this.olLayer.setStyle(this._style);
    }
    /**
     * get the map CRS if it is defined by the map move object
     */
    get mapCrs() {
        return this.mapProj == null ? null : this.mapProj.getCode();
    }
    get mapProj() {
        if (this._projectionMap != null) {
            return this._projectionMap;
        }
        if (this._mapMove) {
            this._projectionMap = this._mapMove.map.getView().getProjection();
            return this._projectionMap;
        }
        else {
            return null;
        }
    }
    /**
     * get the map move object
     * @type {MapMoveCls|*}
     */
    get mapMove() {
        return this._mapMove;
    }
    /**
     * map move params
     * @type {object}
     */
    get mapMoveParams() {
        return this._mapMoveParams;
    }
    get visible() {
        return this._visible;
    }
    /**
     * Set the layer visibility
     * @type {boolean}
     * @override
     */
    set visible(visibility) {
        super.setVisible(visibility);
        if (this._onDemand) {
            this.mapMove.triggerLyrLoad(this);
        }
    }
    /**
     * get the layer vector source
     * @override
     */
    get source() {
        return this.getSource();
    }
    /**
     * array of ol features
     */
    get features() {
        return this.source.getFeatures();
    }
    /**
     *
     */
    get olLayer() {
        return super.getOlLayer();
    }
    setZIndex(newZ) {
        this.olLayer.setZIndex(newZ);
    }
}
exports.LayerBaseVector = LayerBaseVector;
nm.LayerBaseVector = LayerBaseVector;
exports.default = LayerBaseVector;
//# sourceMappingURL=LayerBaseVector.js.map