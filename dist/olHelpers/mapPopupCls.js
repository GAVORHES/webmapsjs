"use strict";
/**
 * Created by gavorhes on 11/3/2015.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const mapInteractionBase_1 = require("./mapInteractionBase");
const provide_1 = require("../util/provide");
const $ = require("jquery");
const Vector_1 = require("ol/layer/Vector");
const Vector_2 = require("ol/source/Vector");
const Overlay_1 = require("ol/Overlay");
const easing = require("ol/easing");
const style_1 = require("ol/style");
const nm = provide_1.default('olHelpers');
class FeatureLayerProperties {
    /**
     *
     * @param feature the feature
     * @param layer - the layer in the popup
     * @param layerIndex - index of the layer
     * @param selectionLayer - the ol selection layer
     * @param [esriLayerName=undefined] - esri layer name
     */
    constructor(feature, layer, layerIndex, selectionLayer, esriLayerName) {
        this.feature = feature;
        this.layer = layer;
        this.layerIndex = layerIndex;
        this.selectionLayer = selectionLayer;
        this.popupContent = '';
        this.esriLayerName = typeof esriLayerName == 'string' ? esriLayerName : undefined;
    }
    get layerName() {
        if (typeof this.esriLayerName == 'string') {
            return this.esriLayerName;
        }
        else {
            return this.layer.name;
        }
    }
}
exports.FeatureLayerProperties = FeatureLayerProperties;
/**
 * map popup class
 * @augments MapInteractionBase
 */
class MapPopupCls extends mapInteractionBase_1.default {
    /**
     * Definition for openlayers style function
     * @callback olStyleFunction
     * &param feature the openlayers vector feature
     * $param
     */
    /**
     * map popup constructor
     */
    constructor() {
        super('map popup');
        this._arrPopupLayerIds = [];
        this._arrPopupLayers = [];
        // this._arrPopupOlLayers = [];
        this._arrPopupContentFunction = [];
        this._$popupContainer = undefined;
        this._$popupContent = undefined;
        this._$popupCloser = undefined;
        this._popupOverlay = undefined;
        this._selectionLayers = [];
        this._selectionLayerLookup = {};
        this._mapClickFunctions = [];
        //let a = function($jqueryContent){console.log($jqueryContent)};
        //this._popupChangedLookup = {'a': a};
        this._popupChangedFunctions = [];
        this._esriMapServiceLayers = [];
        this._popupOpen = false;
        this._popupCoordinate = null;
        this._passThroughLayerFeatureArray = [];
        this._currentPopupIndex = -1;
        this._popupContentLength = 0;
    }
    /**
     * map popup initialization
     * @param  theMap - the ol map
     */
    init(theMap) {
        super.init(theMap);
        let $map;
        let target = this.map.getTarget();
        if (typeof target == 'string') {
            $map = $('#' + target);
        }
        else {
            $map = $(target);
        }
        $map.append('<div class="ol-popup">' +
            '<span class="ol-popup-closer">X</span>' +
            '<div class="popup-content"></div>' +
            '</div>');
        this._$popupContainer = $map.find('.ol-popup');
        this._$popupContent = $map.find('.popup-content');
        this._$popupCloser = $map.find('.ol-popup-closer');
        let _ease = (n) => {
            return easing['inAndOut'](n);
        };
        this._popupOverlay = new Overlay_1.default({
            element: this._$popupContainer[0],
            autoPan: true,
            autoPanAnimation: {
                duration: 250,
                // source: theMap.getView().getCenter(),
                easing: _ease
            }
        });
        this._map.addOverlay(this._popupOverlay);
        this._$popupCloser.click((evt) => {
            this.closePopup();
        });
        // display popup on click
        this._map.on('singleclick', (evt) => {
            this.closePopup();
            this._popupCoordinate = evt['coordinate'];
            // esri map service layers
            if (this._esriMapServiceLayers.length > 0) {
                let queryParams = {
                    geometry: evt['coordinate'].join(','),
                    geometryType: 'esriGeometryPoint',
                    layers: 'all',
                    sr: this._map.getView().getProjection().getCode().split(':')[1],
                    mapExtent: this._map.getView().calculateExtent(this._map.getSize()).join(','),
                    imageDisplay: this._map.getSize().join(',') + ',96',
                    returnGeometry: true,
                    tolerance: 15,
                    f: 'pjson'
                };
                for (let l of this._esriMapServiceLayers) {
                    l.getPopupInfo(queryParams);
                }
            }
            let layerFeatureObjectArray = this._featuresAtPixel(evt['pixel']);
            this._passThroughLayerFeatureArray = [];
            this._currentPopupIndex = -1;
            for (let i = 0; i < layerFeatureObjectArray.length; i++) {
                let featObj = layerFeatureObjectArray[i];
                let props = featObj.feature.getProperties();
                let popupContentResponse = this._arrPopupContentFunction[featObj.layerIndex](props, this._$popupContent);
                //skip if return was false
                if (popupContentResponse === false) {
                    //continue;
                }
                else if (typeof popupContentResponse == 'string') {
                    featObj.popupContent = popupContentResponse;
                    this._passThroughLayerFeatureArray.push(featObj);
                }
                else {
                    featObj.selectionLayer.getSource().addFeature(featObj.feature);
                }
            }
            this._popupContentLength = this._passThroughLayerFeatureArray.length;
            this._currentPopupIndex = -1;
            let popupHtml = '<div class="ol-popup-nav">';
            popupHtml += '<span class="previous-popup ol-popup-nav-arrow">&#9664;</span>';
            popupHtml += '<span class="next-popup ol-popup-nav-arrow">&#9654;</span>';
            popupHtml += `<span class="current-popup-item-number" style="font-weight: bold;"></span>`;
            popupHtml += `<span>&nbsp;of&nbsp;</span>`;
            popupHtml += `<span class="popup-content-length" style="font-weight: bold;">${this._popupContentLength}</span>`;
            popupHtml += `<span>&nbsp;&nbsp;-&nbsp;&nbsp;</span>`;
            popupHtml += `<span class="current-popup-layer-name"></span>`;
            popupHtml += '</div>';
            popupHtml += '<div class="ol-popup-inner">';
            popupHtml += '</div>';
            this._$popupContent.html(popupHtml);
            this._$popupContent.find('.previous-popup').click(() => {
                if (this._popupContentLength == 1) {
                    return;
                }
                if (this._currentPopupIndex == 0) {
                    this._currentPopupIndex = this._popupContentLength - 1;
                }
                else {
                    this._currentPopupIndex--;
                }
                this._triggerFeatSelect();
            });
            let nextPopup = this._$popupContent.find('.next-popup');
            nextPopup.click(() => {
                if (this._popupContentLength == 1 && this._currentPopupIndex > -1) {
                    return;
                }
                if (this._currentPopupIndex == this._popupContentLength - 1) {
                    this._currentPopupIndex = 0;
                }
                else {
                    this._currentPopupIndex++;
                }
                this._triggerFeatSelect();
            });
            if (this._popupContentLength > 0) {
                nextPopup.trigger('click');
                this._popupOverlay.setPosition(this._popupCoordinate);
                this._$popupContent.scrollTop(0);
                this._popupOpen = true;
            }
        });
        //change mouse cursor when over marker
        this._map.on('pointermove', (evt) => {
            if (evt['dragging']) {
                return;
            }
            let pixel = this.map.getEventPixel(evt['originalEvent']);
            let hit = false;
            this.map.forEachFeatureAtPixel(pixel, (f, l) => {
                if (hit) {
                    return;
                }
                for (let vLyr of this._arrPopupLayers) {
                    if (vLyr.olLayer == l) {
                        hit = true;
                        break;
                    }
                }
            });
            let mapElement = this.map.getTargetElement();
            mapElement.style.cursor = hit ? 'pointer' : '';
        });
    }
    /**
     * helper to select features
     * @private
     */
    _triggerFeatSelect() {
        let $currentPopupItemNumber = this._$popupContent.find('.current-popup-item-number');
        let $innerPopup = this._$popupContent.find('.ol-popup-inner');
        let $layerNameSpan = this._$popupContent.find('.current-popup-layer-name');
        this.clearSelection();
        let lyrFeatObj = this._passThroughLayerFeatureArray[this._currentPopupIndex];
        $currentPopupItemNumber.html((this._currentPopupIndex + 1).toFixed());
        $layerNameSpan.html(lyrFeatObj.layerName);
        $innerPopup.html(lyrFeatObj.popupContent);
        lyrFeatObj.selectionLayer.getSource().addFeature(lyrFeatObj.feature);
        for (let f of this._popupChangedFunctions) {
            f(this._$popupContent);
        }
    }
    /**
     *
     * @param feature - the ol feature
     * @param {LayerEsriMapServer} lyr - the map server layer
     * @param {string} popupContent - popup content
     * @param {string} esriName - esri layer name
     */
    addMapServicePopupContent(feature, lyr, popupContent, esriName) {
        let featLayerObject = new FeatureLayerProperties(feature, lyr, this._popupContentLength, this._selectionLayerLookup[lyr.id], esriName);
        featLayerObject.popupContent = popupContent;
        this._passThroughLayerFeatureArray.push(featLayerObject);
        this._popupContentLength++;
        $('.popup-content-length').html(this._popupContentLength.toFixed());
        if (!this._popupOpen) {
            this._$popupContent.find('.next-popup').trigger('click');
            this._popupOverlay.setPosition(this._popupCoordinate);
            this._$popupContent.scrollTop(0);
            this._popupOpen = true;
        }
    }
    /**
     *
     * @param  pixel - the ol pixel
     * @returns  feature layer properties
     * @private
     */
    _featuresAtPixel(pixel) {
        let layerFeatureObjectArray = [];
        this.map.forEachFeatureAtPixel(pixel, (feature, layer) => {
            let hasLyr = false;
            let j;
            let lyr = null;
            for (j = 0; j < this._arrPopupLayers.length; j++) {
                lyr = this._arrPopupLayers[j];
                if (lyr.olLayer === layer) {
                    hasLyr = true;
                    break;
                }
            }
            if (hasLyr) {
                layerFeatureObjectArray.push(new FeatureLayerProperties(feature, lyr, j, this._selectionLayers[j]));
            }
        });
        return layerFeatureObjectArray;
    }
    closePopup() {
        this._checkInit();
        this._popupOpen = false;
        this._popupOverlay.setPosition(undefined);
        this._$popupCloser[0].blur();
        this.clearSelection();
        this._$popupContent.html('');
        return false;
    }
    ;
    /**
     *
     * @param chgFunction - popup change function
     */
    addPopupChangedFunction(chgFunction) {
        this._popupChangedFunctions.push(chgFunction);
    }
    /**
     *
     * @param {LayerBase|*} lyr - the layer being acted on
     * @param {object} [selectionStyle={}] the selection style configuration
     * @param {string} [selectionStyle.color=rgba(255,170,0,0.5)] the selection color
     * @param {number} [selectionStyle.width=10] the selection width for linear features
     * @param {object|function} [selectionStyle.olStyle=undefined] an openlayers style object or function
     * @returns  the new selection layer
     * @private
     */
    _addPopupLayer(lyr, selectionStyle) {
        this._checkInit();
        selectionStyle = selectionStyle || {};
        selectionStyle.color = selectionStyle.color || 'rgba(255,170,0,0.5)';
        selectionStyle.width = selectionStyle.width || 10;
        let theStyle;
        if (selectionStyle.olStyle) {
            theStyle = selectionStyle.olStyle;
        }
        else {
            theStyle = new style_1.Style({
                stroke: new style_1.Stroke({
                    color: selectionStyle.color,
                    width: selectionStyle.width
                }),
                image: new style_1.Circle({
                    radius: 7,
                    fill: new style_1.Fill({ color: selectionStyle.color }),
                    stroke: new style_1.Stroke({ color: selectionStyle.color, width: 1 })
                }),
                fill: new style_1.Fill({
                    color: selectionStyle.color
                })
            });
        }
        let selectionLayer = new Vector_1.default({
            source: new Vector_2.default(),
            style: theStyle
        });
        selectionLayer.setZIndex(100);
        this._selectionLayers.push(selectionLayer);
        this._selectionLayerLookup[lyr.id] = selectionLayer;
        this.map.addLayer(selectionLayer);
        return selectionLayer;
    }
    /**
     * Add popup to the map
     * @param {LayerBase|*} lyr The layer that the popup with act on
     * @param {popupCallback} popupContentFunction - popup content function that makes popup info
     * @param {object} [selectionStyle={}] the selection style configuration
     * @param {string} [selectionStyle.color=rgba(255,170,0,0.5)] the selection color
     * @param {number} [selectionStyle.width=10] the selection width for linear features
     * @param {object|function} [selectionStyle.olStyle=undefined] an openlayers style object or function
     * @returns {object} a reference to the ol selection layer
     */
    addVectorPopup(lyr, popupContentFunction, selectionStyle) {
        let selectionLayer = this._addPopupLayer(lyr, selectionStyle);
        this._arrPopupLayerIds.push(lyr.id);
        this._arrPopupLayers.push(lyr);
        // this._arrPopupOlLayers.push(lyr.olLayer);
        this._arrPopupContentFunction.push(popupContentFunction);
        return selectionLayer;
    }
    ;
    /**
     *
     * @param {LayerBase} lyr - layer
     */
    removeVectorPopup(lyr) {
        let idx = this._arrPopupLayerIds.indexOf(lyr.id);
        if (idx > -1) {
            this._arrPopupLayerIds.splice(idx, 1);
            this._arrPopupLayers.splice(idx, 1);
            // this._arrPopupOlLayers.splice(idx, 1);
            this._arrPopupContentFunction.splice(idx, 1);
            this._selectionLayers.splice(idx, 1);
            delete this._selectionLayerLookup[lyr.id];
        }
    }
    /**
     *
     * @param {LayerEsriMapServer} lyr - map server layer
     * @param {object} [selectionStyle={}] the selection style configuration
     * @param {string} [selectionStyle.color=rgba(255,170,0,0.5)] the selection color
     * @param {number} [selectionStyle.width=10] the selection width for linear features
     * @param {object|function} [selectionStyle.olStyle=undefined] an openlayers style object or function
     * @returns {object} a reference to the ol selection layer
     */
    addMapServicePopup(lyr, selectionStyle) {
        let selectionLayer = this._addPopupLayer(lyr, { olStyle: selectionStyle });
        this._esriMapServiceLayers.push(lyr);
        return selectionLayer;
    }
    clearSelection() {
        this._checkInit();
        for (let i = 0; i < this._selectionLayers.length; i++) {
            this._selectionLayers[i].getSource().clear();
        }
        for (let f of this._mapClickFunctions) {
            f();
        }
    }
    ;
    /**
     * Add a function to be called when the map is clicked but before any popups are implemented
     * @param {function} func - the map click function
     */
    addMapClickFunction(func) {
        this._mapClickFunctions.push(func);
    }
}
exports.MapPopupCls = MapPopupCls;
nm.MapPopupCls = MapPopupCls;
exports.default = MapPopupCls;
//# sourceMappingURL=mapPopupCls.js.map