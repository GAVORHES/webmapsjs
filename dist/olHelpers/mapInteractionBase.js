"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by gavorhes on 12/8/2015.
 */
const provide_1 = require("../util/provide");
const nm = provide_1.default('olHelpers');
/**
 * base interaction
 */
class MapInteractionBase {
    /**
     * map interaction base
     * @param subtype - the interaction subtype
     */
    constructor(subtype) {
        this._map = null;
        this._initialized = false;
        this._subtype = subtype;
    }
    /**
     * base initializer, returns true for already initialized
     * @param theMap - the ol Map
     * @returns true for already initialized
     */
    init(theMap) {
        if (!this._initialized) {
            this._map = theMap;
            this._initialized = true;
        }
    }
    /**
     * get reference to the ol map object
     * @returns {ol.Map} the map object
     */
    get map() {
        return this._map;
    }
    /**
     * get if is initialized
     * @returns {boolean} is initialized
     */
    get initialized() {
        return this._initialized;
    }
    /**
     * Check the initialization status and throw exception if not valid yet
     * @protected
     */
    _checkInit() {
        if (!this.initialized) {
            let msg = `${this._subtype} object not initialized`;
            alert(msg);
            console.log(msg);
            throw msg;
        }
    }
    /**
     * Check the initialization status and throw exception if not valid yet
     */
    checkInit() {
        this._checkInit();
    }
}
exports.MapInteractionBase = MapInteractionBase;
nm.MapInteractionBase = MapInteractionBase;
exports.default = MapInteractionBase;
//# sourceMappingURL=mapInteractionBase.js.map