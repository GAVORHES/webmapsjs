"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const $ = require("jquery");
const makeGuid_1 = require("../util/makeGuid");
const projections_1 = require("./projections");
const Vector_1 = require("ol/layer/Vector");
const Vector_2 = require("ol/source/Vector");
const Circle_1 = require("ol/style/Circle");
const Fill_1 = require("ol/style/Fill");
const Stroke_1 = require("ol/style/Stroke");
const Point_1 = require("ol/geom/Point");
const Feature_1 = require("ol/Feature");
let invalidClass = 'geocoder-invalid';
let geocoderLoadingClass = 'geocoder-loading';
const Style_1 = require("ol/style/Style");
// let testAddress = '65 7th Street, Prairie du Sac, WI';
class Geocode {
    constructor(mapDiv, map) {
        let inputGuid = makeGuid_1.makeGuid();
        let buttonGuid = makeGuid_1.makeGuid();
        this.map = map;
        this.indicationLayer = new Vector_1.default({
            source: new Vector_2.default(),
            style: new Style_1.default({
                image: new Circle_1.default({
                    radius: 12,
                    fill: new Fill_1.default({ color: 'rgba(255,0,0,0.5)' }),
                    stroke: new Stroke_1.default({ color: 'red', width: 1 })
                })
            })
        });
        this.map.addLayer(this.indicationLayer);
        $(mapDiv).append('<div class="geocoder-el">' +
            `<input type="text" id="${inputGuid}">` +
            `<button id="${buttonGuid}">Search</button>` +
            '</div>');
        this.theButton = document.getElementById(buttonGuid);
        this.theInput = document.getElementById(inputGuid);
        this.reset();
        let $theButton = $(this.theButton);
        let $theInput = $(this.theInput);
        $theButton.click((evt) => {
            evt.preventDefault();
            $theButton.addClass(geocoderLoadingClass);
            this.theButton.disabled = true;
            this.indicationLayer.getSource().clear();
            $.get(`https://geocode.xyz/${this.theInput.value}?geoit=json`, {}, (d) => {
                let lat = parseFloat(d['latt']);
                let lon = parseFloat(d['longt']);
                if ((lat == 0 && lon == 0) || d['error']) {
                    $theInput.addClass(invalidClass);
                    this.theInput.title = 'Specified Location Invalid';
                    this.theButton.title = 'Specified Location Invalid';
                }
                else {
                    let v = this.map.getView();
                    let p = new Point_1.default([lon, lat]);
                    let feat = new Feature_1.default(p);
                    this.indicationLayer.getSource().addFeature(feat);
                    p.transform(projections_1.proj4326, projections_1.proj3857);
                    v.setCenter(p.getCoordinates());
                    v.setZoom(13);
                }
                $theButton.removeClass(geocoderLoadingClass);
                this.theButton.disabled = false;
            }, 'json');
        });
        $(this.theInput).keyup((evt) => {
            evt.preventDefault();
            this.theButton.disabled = this.theInput.value.length == 0;
            $theInput.removeClass(invalidClass);
            this.theInput.title = '';
            this.theButton.title = '';
            if (!this.theButton.disabled && evt.keyCode == 13) {
                $theButton.click();
            }
        });
    }
    reset() {
        this.theButton.disabled = true;
        this.theInput.value = '';
    }
}
exports.Geocode = Geocode;
//# sourceMappingURL=geocode.js.map