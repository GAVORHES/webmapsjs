"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Vector_1 = require("ol/layer/Vector");
const Vector_2 = require("ol/source/Vector");
const style_1 = require("ol/style");
const Feature_1 = require("ol/Feature");
const Point_1 = require("ol/geom/Point");
const $ = require("jquery");
const searchIndicator = new Vector_1.default({
    source: new Vector_2.default(),
    zIndex: 25,
    style: new style_1.Style({
        image: new style_1.Circle({
            radius: 14,
            stroke: new style_1.Stroke({
                color: '#000000'
            }),
            fill: new style_1.Fill({
                color: 'rgba(204,36,104,0.6)'
            })
        })
    })
});
function search(searchText, map, extent) {
    searchText = searchText.trim();
    if (!searchText) {
        return;
    }
    $.get('https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates', {
        f: 'json',
        SingleLine: searchText,
        searchExtent: '-92.9,42.4,-86.8,47.1',
        outSR: 102100
    }, (d) => {
        if (d['candidates'] && d['candidates']['length'] > 0) {
            let cand = d['candidates'][0];
            let loc = cand['location'];
            if (isNaN(loc.x) || isNaN(loc.y) || typeof loc.x === 'string' || typeof loc.y === 'string') {
                // pass
            }
            else {
                let ext = cand['extent'];
                map.getView().setCenter([loc.x, loc.y]);
                map.getView().fit([ext.xmin, ext.ymin, ext.xmax, ext.ymax], { size: map.getSize() });
                let p = new Point_1.default([loc.x, loc.y]);
                let f = new Feature_1.default(p);
                searchIndicator.getSource().addFeature(f);
                setTimeout(() => {
                    searchIndicator.getSource().clear();
                }, 3000);
            }
        }
    }, 'json');
}
function addSearch(map, xMin = -92.9, yMin = 42.4, xMax = -86.8, yMax = 47.1) {
    map.addLayer(searchIndicator);
    let searchContainer = document.createElement('div');
    searchContainer.style.position = 'absolute';
    searchContainer.style.left = '75px';
    searchContainer.style.top = '15px';
    // searchContainer.style.backgroundColor = 'white';
    searchContainer.style.zIndex = '25';
    searchContainer.id = "search-container";
    let searchText = document.createElement('input');
    searchText.type = 'text';
    searchText.style.height = '25px';
    searchText.style.width = '200px';
    searchText.style.borderRadius = '5px 0 0 5px';
    searchText.style.border = 'solid black 1px';
    searchText.style.padding = '0 0 0 5px';
    searchText.style.verticalAlign = 'top';
    searchText.id = "search-container-text";
    let searchButton = document.createElement('input');
    searchButton.type = 'button';
    searchButton.style.height = '29px';
    searchButton.style.width = '35px';
    searchButton.style.display = 'inline-block';
    searchButton.style.padding = '3px';
    searchButton.style.borderRadius = '0 5px 5px 0';
    searchButton.style.background = "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIwAAACNCAMAAABSQmKpAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAMAUExURQAAAAEAAQMBAgICAgMDAwQCAwUDBAQEBAUEBQcFBgYGBgcHBwgGBwkHCAgICAkICQsJCgoKCgsLCwwKCw0LDAwMDA0MDQ8NDg4ODg8PDxAODxEPEBAQEBEQERMREhISEhMTExQSExUTFBQUFBUUFRcVFhYWFhcXFxgWFxkXGBgYGBkYGRsZGhoaGhsbGxwaGx0bHBwcHB0cHR8dHh4eHh8fHyAeHyEfICAgICEgISMhIiIiIiMjIyQiIyUjJCQkJCUkJSclJiYmJicnJygmJyknKCgoKCkoKSspKioqKisrKywqKy0rLCwsLC0sLS8tLi4uLi8vLzAuLzEvMDAwMDEwMTMxMjIyMjMzMzQyMzUzNDQ0NDU0NTc1NjY2Njc3Nzg2Nzk3ODg4ODk4OTs5Ojo6Ojw6Oz07PDw8PD08PT89Pj4+Pj8/P0A+P0E/QEFAQUNBQkJCQkNDQ0RCQ0VERUdFRkZGRkdHR0hGR0lHSEhISEpISUxKS0xMTE9NTlFPUFNRUlJSUlNTU1RSU1VTVFZUVVdVVldXV1hWV1lXWFpYWVtbW11bXF1cXV9dXl9fX2BeX2FfYGFgYWRiY2RkZGVkZWdlZmZmZmdnZ2hmZ2lnaGhoaGloaWtpampqamtra2xqa21rbAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOgJ4aIAAAEAdFJOU////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////wBT9wclAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGHRFWHRTb2Z0d2FyZQBwYWludC5uZXQgNC4xLjb9TgnoAAAMZUlEQVR4Xu2b/Xvbxn3AaTdWJVeyZEumZMnUCwW+CSAB3AEH3AEHHAhQoF4sx268JHOSJnXaxVk7Z1u3vuylW9OmaVevdbd0Xd/7l15B8kxL1itAUuoP/TyP/fD7PYD++HA4fA8Ac/zPiFQyP/n06f+LjyPhTDLP3p/Pzc3kJaW2vDi7vDyWm373F6JpqJwu89bliaIMYhVgiiEigJLSX1XnS5OXidhgeJwi8974mIzalADLw4gEEYW7AcEBYwzugplLD8RmQ+JEGelSFeA9oFieFmGPYQBwAAHZAR5RkIWpVZqfFJsOhRNkFi8BQimEjAYBYJRYEEJNRp4HYZLxCMMkskD5C2LzIXCsDB5vWYkH9axqC1m16bGJXGnvzfda+YnclcWiggn2QCuuBdurt+pil4E5RuZfJtfaGmCEEMRqkzPxU5EX/PGvl8ZmAdS8gK7HTbnwusgPyNEypRmvBdhdjcno8ux/i+TLyLkShBgzZ1O/fU3kBuMomd9eKd1Xk/NHVbWZd0XuSD65tEbu6GWXGf7Nr4jcIBwh83hJg55FPFQrfF2kjuXZxNU9BlwsG7caIjUAh2WWp7WdWNtD1pW/EZkT+WVOsjcZ9lll8NPqkMxlEGtRgKRFJBKnYpcrjuu6uHxdJDLzsowECaLAsuY/Fokz8NlV27Rt39WPGoBpeGn/fNHBIAT1qyI+IzfrtuG6zfVpEWfkoIy8HNivGnZFFvGZCdexaTvOrdsizsYBmfvzgYfXNzdCEafg4YYJ/Katr4g4E/tlfjVPomIYL+yJOBVfqtfNLdAsDDLf7JeZlOV4R5eBCFPyFclx/YXwpgizsE9GWmvFEDc2RJiae7KOH5rGwVGYihe7fpxnFrDgAJPFgmGbje3bhgjT80Im11IxQ3kRZWIcY1ePb4goPX0ZXPI8jS7+hwizIcnN+l4l82zTl5l8oFgRrYooI75h46ZpPBFhWp7LSJrCPGtMRJm56mw6jea4iNLyXGYq0nYl5SMRZeZp3gZNR/qlCFMiZIoQBkye6wWDkHfs2DQyVhNC5hIiEEun1lJn4Fpou05BBCnpyTyGjETabPfzgFwHbgj0bGVfT2YcQQblE+vdM7NiYux8XgTp6MnMqm2Ch9IxyTcCjL15EaSjK/OhtlOy1CEM3w73ZRrjaqYJqyuTUwKPap92E4OzaFHF6/V4Sro7je8iaA3pKHWOU0SsoghS0ZH5xaIKYzTVSwyOVQsCTclySejI/B2NGIBRLzEEiipsqTURpKEjs6hgQtaOW1KnZ8miEcxymevI5CgFbLEXD4NLpF1iWe4idWQm1B3Fu9yLh8EYTno6y0TRlbECSCd68TAoIExJ1p6Z9jC2Oh+GREuzGMky63Uclj1E4XIvHgZPFBQQKUMBm8h8VoOEgpZIDIEf1JBHao9ElIJE5mlVpRTeF4kh8DOJYJLlCxOZZ2uIEPCWSAyBn60FiJTuiigFicyvlgKE1SHefP9JoQ0JzHaY+Ly6q7AhFRAd3oYIRkvPRJSCrkxLA96VXjwM9ghBlvI/IkpBR2a85e0onQ9DoqYC4mVZJ3ccpqogigZev70gR/YiJcu81ZHJxW2oZqtaj2QaqNZulp7u7DPn1VCr+lkvMQSmgpigGRGkoSNzH5GAgKHNer+RECZ4TURp6Mg8lbEWWUMrOyMrqSCULJNo99BOIkq0oVVXuYhSkuUo9WTGaaRFQ5v1ZEnFrUzTVlcmWm4RqqS+E3007wJtB9cy1WpdGT7FKIh6Hwcmp3kxKf5cRKnoGYxBRtjSv3Y/D8pECyuZTuznMverHiUo692vA8xogeVVl0SUDnFsigqi1vInvWAg5vcgipZ+K6J0CJkljVqMDmHU3NYszJSM6/bn//5C3LDDGz8UUWb+UG3qrzbKGb/nucwtEIe6kfKZ12EK1UZcMQe99frraafBXLYqwox8OBM5MZb+TYRp6Q8TvYyb9gbIND/0KURIN3Dm/n0xZhd8c9PZGOgp45wF0bZ+I+Mt6f0yXy44d2zHkESYgd0SbGsNnP1p+wsZnsP1yjYumyJMzbeXkWrFYICTYJ8Mv67fA4a5nvGVjx8tKa0YwvL/ijgD+2U+qLg6Y7q0LeJUfFdeCghi2hsizsJ+GQ7WTbvhO0aGx7c/V0psF8BA3RSJLByQ4Texy7Btrqd+seuf5iTUohoNFAmLVAYOyvAbZVf39biQ8uWcdhW2GYTUw5YuZXhEL3hJhl+TYjc2sJn7nUichStaFVmEsABqre2GmfEBz2EZfm3Frjd15pR1kTiV9yYwRXuABQRbVuAbejPrReWQDF9Ipi3mVmR7/HsicyJ/nK9BTFk1YAQiLxlxsbNhZBw3h2W4VHFtu9m0wXr+9Il9QypasYoiAmQUeMniDftmUzds0ZyOI2T4o1v1pq1vAcdc/9zvRe5o5qYth+nyll8u3Htt1bS3DUZJsoSztM5Sw+ttdHaOkuH8qgxCB7t6aKzmdkXuEA9m58Bdre5uGWb9lf/j3Cw4juMmfUMIhhrjk1Np76QdLcM3r5V9Wb5nNnx1bemKJbL7sKYWVQwRwz4I9ZVKN7fprGyZiQlJ/lhEgUEhZd8cI5OMYyn0dfyqvZsMhOLU7PTlefrVf//0k4/fauPLM2MyVJWkD5hlbZr58Z+KfTZsB9NEhSR/UZUhUntTtJyNY2X4f16TmF/AtRZGQRQAmnS9KldLEKmlzjj1Wi1Fa1Fw6wvfETskrJaTbhE6LStQrXwqm+NlEi7dcptBlJwrCAYWgpoGCZMpjVmEPAsBryXPzvyX2LaHvdG5BdE5UgiToG21Z7ZEy1k4UYbzem4JxEShAaIsijxiIeohCjCitKXUpvOH3t5uaLAzgAkJYm2NYg82Ulw5T5FJKOWKUIMBQsn/mXosSNZFHkG1mfErPxZbHEDWkgtDsiVAUTIlUwxSXHVPl0n4WBkby89Wl4tQUkq12fnxybXjbzm3VcIsGHQGTgcHS2eu1s4kI/j+40fvf/Ts1Ht/EiLWLhQuRG/Wy2ctuNLInIk/TJG71KrGwoVs6j7LvyMaT2HoMl+cJqqy5/V7xrVdoy7dE60nM3QZ/nbJYq1qS7gQ7Dg23gBnOlLDl0nKPgXG/Z5J1qmbLlsvnMVmBDL80RLF/Z4JKw270Qzx9X8QrScwChn+9lzw4mx6rc5C3fTd6dPXHCOR4Q8XHNtnm7bj6sKJtNDcqY/DRiPD7y24ody0mb4tXAhkwdT7ovU4RiTD37gpx2bZfK0uXJLrQ1SbOOUNs1HJ8K8VDPYA60C4EPVBdYfNn1z7jUyGv7Mqs7r9unBJgMyqFU98rWZ0Mvx1yfcdSZgQAlhSqEW5k562jFCGb63XHyQ1cQ+McbRTZez6B6L1CEYpw7+8YHR+CNTFwyCotpNS8aYvWg8zUhn+zg0XtTzVCpRIOBEzXD32qjlaGf7PNxhR7gbY04QLSS7ix1YUI5bhbBa3ayW006/87lWcMPc10foSo5bhX1+U32QUq8KFgLCxja89FK0HGbkMf2+KesW97mKqg2O7mG2sHllRjF6Gf+NKTGp9maZvMuNVe/yoq+Y5yPAvLsEdT7gQA1fCsKzH146Yb85Dhj+axcliqoeDQ1/eYmYzf/gMPxcZ/sGCbLjbtnlHt4UTga35Qy9wno8M/0Zh213dchywKVyIxtD8y/XNOcnwO6/g7Qpz66FwIbEc7Yx9KFoF5yXDn0jYDJNli3AhICpCNf8t0drj3GT4w4LjNu71a2Ir2o13S7MHbtKdnwz/2xv1uNwfM5QqQN7F83dEa4dzlOEfrDRxv76hbbBzV1Hbt/ddGc5Thr9z1ekPYEw8S25T5O6b/c5Vhj+56gLSViyPvrg8OK/0r1PnK8Of5Hehdj9WGBIupH4nLD+v/c5Zhn9pQo1rQI36lZ/PKkB+3Gs8bxn+YEndiyIsCxeysb2l26/02s5dhn80q1q1vX6hHlfceIN9rtt0/jL8H6d2vGJfxojNhuv3nj9fgAz/6iJudW5ddzFC29nE5d90Gi5ChkdzQX8NzoDPbHul+5LLhcjwx9ftuOG4DjOxzTDY3Ch30xcjw//+OrjDnA3DCcMK3nLD3i/FLkiGfzPv15sP3NDw3U1HNno/kb0oGR6v+Lqp32norumb4k3mC5Phb6zYeqiH/pZfr4vH1hcnw5/cXtG3DAM3bj9/4+YCZTgv37xRrudevC52oTIJBx7kXbTMAf4icxx/kTmOPyMZzv8EzQNFhFeMxUsAAAAASUVORK5CYII=') no-repeat";
    searchButton.style.backgroundSize = '21px 21px';
    searchButton.style.backgroundPosition = '5px 3px';
    searchButton.style.border = 'solid black 1px';
    searchButton.style.cursor = 'pointer';
    searchButton.style.backgroundColor = 'white';
    searchButton.title = 'Search';
    searchButton.id = "search-container-button";
    searchButton.onclick = () => {
        search(searchText.value, map, `${xMin},${yMin},${xMax},${yMax}`);
    };
    searchText.onkeyup = (e) => {
        if (e['keyCode'] === 13) {
            search(searchText.value, map, `${xMin},${yMin},${xMax},${yMax}`);
        }
    };
    searchContainer.appendChild(searchText);
    searchContainer.appendChild(searchButton);
    map.getTargetElement().appendChild(searchContainer);
}
exports.addSearch = addSearch;
//# sourceMappingURL=search.js.map