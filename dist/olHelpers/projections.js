"use strict";
/**
 * Created by gavorhes on 10/3/2016.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const Projection_1 = require("ol/proj/Projection");
exports.proj4326 = new Projection_1.default({ code: 'EPSG:4326' });
exports.proj3857 = new Projection_1.default({ code: 'EPSG:3857' });
exports.proj3070 = new Projection_1.default({ code: 'EPSG:3070' });
//# sourceMappingURL=projections.js.map