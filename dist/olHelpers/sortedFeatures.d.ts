import Feature from 'ol/Feature';
export declare class SortedFeatures {
    sortedFeatures: Feature[];
    propertyName: string;
    _propertyType: string;
    constructor(features: Feature[], propertyName: string);
    getFeature(propertyValue: number | string, exactMatch?: boolean, sortedFeatures?: Array<Feature>): Feature;
}
export default SortedFeatures;
