"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by gavorhes on 7/18/2016.
 */
const provide_1 = require("../util/provide");
const nm = provide_1.default('util');
function calculateExtent(layers) {
    "use strict";
    let hasExtent = false;
    let minX = 10E100;
    let minY = 10E100;
    let maxX = -10E100;
    let maxY = -10E100;
    for (let lyr of layers) {
        let olLayer = lyr.olLayer || lyr;
        if (olLayer.getSource().getFeatures().length > 0) {
            hasExtent = true;
            let ext = olLayer.getSource().getExtent();
            minX = ext[0] < minX ? ext[0] : minX;
            minY = ext[1] < minY ? ext[1] : minY;
            maxX = ext[2] > maxX ? ext[2] : maxX;
            maxY = ext[3] > maxY ? ext[3] : maxY;
        }
    }
    if (hasExtent) {
        return [minX, minY, maxX, maxY];
    }
    else {
        return undefined;
    }
}
exports.calculateExtent = calculateExtent;
nm.calculateExtent = calculateExtent;
/**
 * given one or an array of layers, fit to the map
 * @param layers - array of layers or single
 * @param  mp - the map to fit
 * @param [zoomOut=undefined] - levels to zoom out after fit
 */
function fitToMap(layers, mp, zoomOut) {
    "use strict";
    let ext = calculateExtent(layers);
    if (typeof ext == 'undefined') {
        return;
    }
    mp.getView().fit(ext, { size: mp.getSize() });
    if (typeof zoomOut == 'number') {
        mp.getView().setZoom(mp.getView().getZoom() - zoomOut);
    }
}
exports.fitToMap = fitToMap;
nm.calculateExtent = calculateExtent;
//# sourceMappingURL=extentUtil.js.map