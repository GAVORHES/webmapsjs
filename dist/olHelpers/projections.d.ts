/**
 * Created by gavorhes on 10/3/2016.
 */
import Projection from 'ol/proj/Projection';
export declare const proj4326: Projection;
export declare const proj3857: Projection;
export declare const proj3070: Projection;
