"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by gavorhes on 12/23/2015.
 */
const provide_1 = require("../util/provide");
let nm = provide_1.default('olHelpers');
class SortedFeatures {
    constructor(features, propertyName) {
        this.sortedFeatures = features;
        this.propertyName = propertyName;
        if (this.sortedFeatures.length > 0) {
            this._propertyType = typeof this.sortedFeatures[0].getProperties()[this.propertyName];
            // let __this = this;
            this.sortedFeatures.sort((a, b) => {
                if (this._propertyType == 'number') {
                    let aMinusB = a.getProperties()[this.propertyName] - b.getProperties()[this.propertyName];
                    if (aMinusB == 0) {
                        return 0;
                    }
                    else {
                        return aMinusB > 0 ? 1 : -1;
                    }
                }
                else if (this._propertyType == 'string') {
                    let propA = a.getProperties()[this.propertyName] || '';
                    let propB = b.getProperties()[this.propertyName] || '';
                    propA = propA.toString().trim();
                    propB = propB.toString().trim();
                    if (propA == propB) {
                        return 0;
                    }
                    else {
                        return propA > propB ? 1 : -1;
                    }
                }
            });
        }
    }
    getFeature(propertyValue, exactMatch = false, sortedFeatures) {
        if (typeof sortedFeatures == 'undefined') {
            sortedFeatures = this.sortedFeatures;
        }
        if (sortedFeatures.length == 0) {
            return undefined;
        }
        if (sortedFeatures.length == 1) {
            if (exactMatch) {
                if (sortedFeatures[0].getProperties()[this.propertyName] == propertyValue) {
                    return sortedFeatures[0];
                }
                else {
                    return undefined;
                }
            }
            else {
                return sortedFeatures[0];
            }
        }
        let lowProp = sortedFeatures[0].getProperties()[this.propertyName];
        let highProp = sortedFeatures[sortedFeatures.length - 1].getProperties()[this.propertyName];
        if (exactMatch) {
            if (lowProp == propertyValue) {
                return sortedFeatures[0];
            }
            else if (propertyValue < lowProp) {
                return undefined;
            }
            else if (highProp == propertyValue) {
                return sortedFeatures[sortedFeatures.length - 1];
            }
            else if (propertyValue > highProp) {
                return undefined;
            }
        }
        else {
            if (propertyValue <= lowProp) {
                return sortedFeatures[0];
            }
            else if (propertyValue >= highProp) {
                return sortedFeatures[sortedFeatures.length - 1];
            }
        }
        let midIndex = Math.floor(sortedFeatures.length / 2);
        let midFeature = sortedFeatures[midIndex];
        let midProperty = midFeature.getProperties()[this.propertyName];
        if (midProperty === propertyValue) {
            return midFeature;
        }
        if (propertyValue < midProperty) {
            return this.getFeature(propertyValue, exactMatch, sortedFeatures.slice(0, midIndex));
        }
        else {
            return this.getFeature(propertyValue, exactMatch, sortedFeatures.slice(midIndex));
        }
    }
}
exports.SortedFeatures = SortedFeatures;
nm.SortedFeatures = SortedFeatures;
exports.default = SortedFeatures;
//# sourceMappingURL=sortedFeatures.js.map