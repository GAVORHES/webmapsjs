/**
 * Created by glenn on 6/12/2017.
 */
import React = require("react");
import 'jquery-ui';
export interface iDateRange {
    maxRange: number;
    callback: (start: Date, end: Date, version?: number) => any;
    minRange: number;
    maxDate: Date;
    minDate: Date;
    initialEnd: Date;
    start: Date;
    end: Date;
    npmrds: boolean;
}
/**
 * Date range widget
 *
 * maxRange: number - max number of days
 * callback: (start: Date, end: Date, version?: number) => any - callback function
 * minRange: number (*optional)
 * minDate: Date (*optional) - minimum date
 * maxDate: Date (*optional) - maximum date
 * initialEnd: Date (*optional) - initial, defaults to end date
 * start: Date (*optional) - start date
 * end: Date (*optional) - end date
 * npmrds: boolean (*optional) - is for npmrds
 */
export declare class DateRange extends React.Component<iDateRange, {}> {
    startId: string;
    endId: string;
    startInput: HTMLInputElement;
    endInput: HTMLInputElement;
    start: Date;
    end: Date;
    maxRange: number;
    minRange: number;
    numDays: number;
    versionTwoStart: Date;
    previousStart: Date;
    previousEnd: Date;
    constructor(props: iDateRange, context: Object);
    setNumDays(): void;
    componentDidMount(): void;
    private readonly needReset;
    private readonly versionSpan;
    private readonly version;
    private finalizeChange;
    private setStart;
    private setEnd;
    render(): JSX.Element;
}
