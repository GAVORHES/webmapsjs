"use strict";
/**
 * Created by glenn on 6/14/2017.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const $ = require("jquery");
window['$'] = window['jQuery'] = $; // notice the definition of global variables here
require("jqueryui");
const makeGuid_1 = require("../util/makeGuid");
const dateFormat_1 = require("./helpers/dateFormat");
/**
 * params label, id, initialDate, change callback with value as string
 */
class DatePick extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.elId = this.props.id || makeGuid_1.default();
    }
    componentDidMount() {
        let $el = $('#' + this.elId);
        $el.datepicker({
            onSelect: () => {
                this.props.change(dateFormat_1.stringToDate($el.val()));
            }
        });
    }
    render() {
        let params = {
            id: this.elId,
            type: 'text',
            // style: {margin: "0 10px 0 5px", width: '73px'},
            readOnly: true
        };
        if (this.props.val) {
            params['value'] = dateFormat_1.dateToString(this.props.val);
        }
        else {
            params['defaultValue'] = dateFormat_1.dateToString(this.props.initialDate || new Date());
        }
        return React.createElement("span", { className: "date-pick" },
            React.createElement("label", { htmlFor: this.elId }, this.props.label),
            React.createElement("input", Object.assign({ style: { textAlign: 'center', margin: "0 10px 0 5px", width: '73px' } }, params)));
    }
}
exports.DatePick = DatePick;
exports.default = DatePick;
//# sourceMappingURL=DatePick.js.map