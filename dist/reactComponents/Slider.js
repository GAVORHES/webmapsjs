"use strict";
/**
 * Created by glenn on 7/6/2017.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const makeGuid_1 = require("../util/makeGuid");
const get_browser_1 = require("../util/get_browser");
class Slider extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.uid = makeGuid_1.default();
        this.startUid = makeGuid_1.default();
        this.stopUid = makeGuid_1.default();
        this.intervalUid = makeGuid_1.default();
        this.previousUid = makeGuid_1.default();
        this.nextUid = makeGuid_1.default();
        this.running = false;
    }
    componentDidMount() {
        this.el = document.getElementById(this.uid);
        this.minVal = parseFloat(this.el.min);
        this.maxVal = parseFloat(this.el.max);
        this.step = parseFloat(this.el.step);
        this.startButton = document.getElementById(this.startUid);
        this.stopButton = document.getElementById(this.stopUid);
        if (this.props.animate) {
            this.stopButton.style.display = 'none';
        }
        this.previousButton = document.getElementById(this.previousUid);
        this.nextButton = document.getElementById(this.nextUid);
        this.intervalSelect = document.getElementById(this.intervalUid);
        if (get_browser_1.get_browser().name.toUpperCase().indexOf('IE') > -1) {
            this.el.onchange = (e) => {
                this.props.change(parseFloat(e.target['value']));
            };
        }
    }
    updateRunning() {
        this.el.disabled = this.running;
        this.startButton.style.display = this.running ? 'none' : '';
        this.stopButton.style.display = this.running ? '' : 'none';
        this.nextButton.disabled = this.running;
        this.previousButton.disabled = this.running;
    }
    startAnimate() {
        this.running = true;
        this.updateRunning();
        this.interval = setInterval(() => {
            let val = parseFloat(this.el.value);
            val += this.step;
            if (val > this.maxVal) {
                val = this.minVal;
            }
            this.el.value = val.toString();
            this.props.change(val);
        }, parseInt(this.intervalSelect.value));
    }
    stopAnimate() {
        clearInterval(this.interval);
        this.running = false;
        this.updateRunning();
    }
    restartAnimate() {
        if (this.running) {
            this.stopAnimate();
            this.startAnimate();
        }
    }
    increment(v) {
        let val = parseFloat(this.el.value);
        val = v > 0 ? val + this.step : val - this.step;
        this.el.value = val.toString();
        this.props.change(val);
    }
    render() {
        let attrs = {
            id: this.uid,
            min: 0,
            type: 'range',
            onChange: (evt) => {
                this.props.change(parseFloat(evt.target.value));
            },
            style: { width: '100%', padding: '4px 0' },
            max: "100",
            step: '0.1',
            value: this.props.value ? this.props.value.toString() : '0',
            defaultValue: "0"
        };
        if (this.props.steps) {
            attrs.max = this.props.steps.toString();
            attrs.step = '1';
        }
        if (this.props.value) {
            delete attrs.defaultValue;
        }
        else {
            delete attrs.value;
        }
        let start = null;
        let stop = null;
        let previous = null;
        let next = null;
        let intervalSelect = null;
        let interval = "200";
        if (this.props.defaultAnimationInterval) {
            interval = this.props.defaultAnimationInterval.toFixed();
        }
        if (this.props.animate) {
            previous = React.createElement("button", { id: this.previousUid, className: "react-slider-previous", onClick: () => {
                    this.increment(-1);
                }, title: "Previous" });
            next = React.createElement("button", { id: this.nextUid, className: "react-slider-next", onClick: () => {
                    this.increment(1);
                }, title: "Next" });
            start = React.createElement("button", { id: this.startUid, className: "react-slider-start", onClick: () => {
                    this.startAnimate();
                }, title: "Start" });
            stop = React.createElement("button", { id: this.stopUid, className: "react-slider-stop", onClick: () => {
                    this.stopAnimate();
                }, title: "Stop" });
            intervalSelect = React.createElement("span", null,
                React.createElement("label", { style: { fontWeight: 'bold', marginRight: '3px' } }, "Interval (s)"),
                React.createElement("select", { defaultValue: interval, id: this.intervalUid, onChange: () => {
                        this.restartAnimate();
                    } },
                    React.createElement("option", { value: "100" }, "0.1"),
                    React.createElement("option", { value: "200" }, "0.2"),
                    React.createElement("option", { value: "300" }, "0.3"),
                    React.createElement("option", { value: "400" }, "0.4"),
                    React.createElement("option", { value: "500" }, "0.5"),
                    React.createElement("option", { value: "600" }, "0.6"),
                    React.createElement("option", { value: "700" }, "0.7"),
                    React.createElement("option", { value: "800" }, "0.8"),
                    React.createElement("option", { value: "900" }, "0.9"),
                    React.createElement("option", { value: "1000" }, "1.0")));
        }
        return React.createElement("div", { className: "react-slider" },
            React.createElement("input", Object.assign({}, attrs)),
            React.createElement("div", { className: "react-slider-controls", style: { textAlign: 'center' } },
                previous,
                start,
                stop,
                next,
                intervalSelect));
    }
}
Slider.defaultProps = {
    steps: null,
    animate: false,
    defaultAnimationInterval: null,
    value: null
};
exports.Slider = Slider;
//# sourceMappingURL=Slider.js.map