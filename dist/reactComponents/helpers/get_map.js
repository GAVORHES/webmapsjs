"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function default_1(map, layer) {
    return new Promise((resolve, reject) => {
        if (typeof map == 'function') {
            let getMap = map;
            let g = setInterval(() => {
                let m = getMap();
                if (m) {
                    m.addLayer(layer);
                    clearInterval(g);
                    resolve(m);
                    /*                    console.log(m);
                                        return m;*/
                }
            }, 15);
        }
        else {
            let m = map;
            m.addLayer(layer);
            resolve(m);
        }
    });
}
exports.default = default_1;
//# sourceMappingURL=get_map.js.map