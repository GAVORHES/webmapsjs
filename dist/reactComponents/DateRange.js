"use strict";
/**
 * Created by glenn on 6/12/2017.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
require("jquery-ui");
const makeGuid_1 = require("../util/makeGuid");
const fixDate = require("./helpers/dateFormat");
const DatePick_1 = require("./DatePick");
function stringToDate(dte) {
    if (dte.getTime) {
        return dte;
    }
    else {
        return new Date(dte);
    }
}
/**
 * Date range widget
 *
 * maxRange: number - max number of days
 * callback: (start: Date, end: Date, version?: number) => any - callback function
 * minRange: number (*optional)
 * minDate: Date (*optional) - minimum date
 * maxDate: Date (*optional) - maximum date
 * initialEnd: Date (*optional) - initial, defaults to end date
 * start: Date (*optional) - start date
 * end: Date (*optional) - end date
 * npmrds: boolean (*optional) - is for npmrds
 */
class DateRange extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.startId = makeGuid_1.default();
        this.endId = makeGuid_1.default();
        this.versionTwoStart = new Date(2017, 1, 1);
        this.maxRange = Math.round(this.props.maxRange) - 1;
        this.minRange = typeof this.props['minRange'] == 'number' ? Math.round(this.props['minRange']) : 1;
        if (this.minRange > this.maxRange) {
            throw "DateRange component: Max range must be greater than min range";
        }
        if (this.props.initialEnd) {
            this.end = stringToDate(this.props.initialEnd);
        }
        else {
            this.end = new Date();
        }
        this.end.setHours(0, 0, 0);
        this.start = new Date(this.end.getTime());
        this.start.setDate(this.start.getDate() - this.maxRange);
        this.setNumDays();
    }
    setNumDays() {
        this.numDays = Math.round((this.end.getTime() - this.start.getTime()) / (1000 * 60 * 60 * 24)) + 1;
    }
    componentDidMount() {
        this.startInput = document.getElementById(this.startId);
        this.endInput = document.getElementById(this.endId);
        this.props.callback(this.start, this.end, this.version);
    }
    get needReset() {
        return this.numDays > this.maxRange || this.numDays < this.minRange;
    }
    get versionSpan() {
        if (this.start < this.versionTwoStart && this.end >= this.versionTwoStart) {
            return true;
        }
        else if (fixDate.dateToString(this.versionTwoStart) === fixDate.dateToString(this.end) && this.start < this.versionTwoStart) {
            return true;
        }
        return false;
    }
    get version() {
        if (fixDate.dateToString(this.start) == fixDate.dateToString(this.versionTwoStart)) {
            return 2;
        }
        else if (this.start >= this.versionTwoStart) {
            return 2;
        }
        return 1;
    }
    finalizeChange() {
        if (this.props.npmrds) {
            if (this.versionSpan) {
                this.start = this.previousStart;
                this.end = this.previousEnd;
                this.startInput.value = fixDate.dateToString(this.start);
                this.endInput.value = fixDate.dateToString(this.end);
                this.setNumDays();
                alert("Start and End dates must not span version break: " + fixDate.dateToString(this.versionTwoStart));
                return;
            }
        }
        this.props.callback(this.start, this.end, this.version);
    }
    setStart(s) {
        this.previousStart = new Date(this.start.getTime());
        this.previousEnd = new Date(this.end.getTime());
        this.start = s;
        this.setNumDays();
        if (this.needReset) {
            this.end = new Date(this.start.getTime());
            if (this.numDays > this.maxRange) {
                this.end.setDate(this.end.getDate() + this.maxRange);
            }
            else {
                this.end.setDate(this.end.getDate() + this.minRange - 1);
            }
            this.endInput.value = fixDate.dateToString(this.end);
            this.setNumDays();
        }
        this.finalizeChange();
    }
    setEnd(s) {
        this.previousStart = new Date(this.start.getTime());
        this.previousEnd = new Date(this.end.getTime());
        this.end = s;
        this.setNumDays();
        if (this.needReset) {
            this.start = new Date(this.end.getTime());
            if (this.numDays > this.maxRange) {
                this.start.setDate(this.start.getDate() - this.maxRange);
            }
            else {
                this.start.setDate(this.start.getDate() - this.minRange + 1);
            }
            this.startInput.value = fixDate.dateToString(this.start);
            this.setNumDays();
        }
        this.finalizeChange();
    }
    render() {
        return React.createElement("div", { className: "date-range" },
            React.createElement(DatePick_1.default, { id: this.startId, label: "Start", initialDate: this.start, change: (s) => { this.setStart(s); } }),
            React.createElement(DatePick_1.default, { id: this.endId, label: "End", initialDate: this.end, change: (s) => { this.setEnd(s); } }));
    }
}
exports.DateRange = DateRange;
//# sourceMappingURL=DateRange.js.map