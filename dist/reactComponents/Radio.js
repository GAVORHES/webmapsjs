"use strict";
/**
 * Created by glenn on 6/12/2017.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
require("jquery-ui");
const makeGuid_1 = require("../util/makeGuid");
class RadioItem extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.guid = makeGuid_1.default();
    }
    render() {
        let style = {};
        if (this.props.inline) {
            style = {
                display: 'inline-block',
                padding: '0 5px'
            };
        }
        let props = {
            id: this.guid,
            type: "radio",
            name: this.props.groupId,
            value: typeof this.props.index == 'undefined' ? this.props.text : this.props.index.toFixed(),
            onChange: (evt) => {
                this.props.change(evt.target.value);
                evt.target.checked = true;
            },
            checked: this.props.checked,
            defaultChecked: this.props.checked
        };
        if (this.props.connected) {
            delete props.defaultChecked;
        }
        else {
            delete props.checked;
        }
        return React.createElement("li", { style: style },
            React.createElement("input", Object.assign({}, props)),
            React.createElement("label", { htmlFor: this.guid }, this.props.text));
    }
}
class RadioBase extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.inline = this.props.inline || false;
        this.groupId = this.props.title.toLowerCase().replace(/ /g, '');
    }
    render() {
        let arr = [];
        for (let i = 0; i < this.props.items.length; i++) {
            let itemProps = {
                groupId: this.groupId,
                text: this.props.items[i],
                inline: this.props.inline,
                change: (s) => (this.props.callback(s)),
                key: this.props.items[i],
                connected: this.props.connected || false,
                checked: false,
                index: i
            };
            if (typeof this.props.selectedValueOrIndex == 'number') {
                itemProps.checked = i == this.props.selectedValueOrIndex;
            }
            else {
                itemProps.checked = this.props.items[i] == this.props.selectedValueOrIndex;
                delete itemProps.index;
            }
            arr.push(React.createElement(RadioItem, Object.assign({}, itemProps)));
        }
        let classes = ['radio-list'];
        if (this.props.classes) {
            classes = classes.concat(this.props.classes);
        }
        return React.createElement("div", { className: classes.join(' ') },
            React.createElement("h4", null, this.props.title),
            React.createElement("ul", { style: { listStyle: 'none' } }, arr));
    }
}
//
// interface _iRadioBaseOpt {
//     inline?: boolean;
//     classes?: string[];
// }
//
// interface _iRadioBase extends _iRadioBaseOpt{
//     title: string;
//     items: string[];
//     callback: (val: string) => any;
//     connected: boolean;
// }
//
// interface iRadioBase extends _iRadioBase{
//     selectedValueOrIndex: string|number;
// }
// interface iRadio {
//     title: string;
//     items: string[];
//     callback: (val: string) => any;
//     inline?: boolean;
//     classes?: string[];
// }
class Radio extends React.Component {
    render() {
        return React.createElement(RadioBase, { title: this.props.title, items: this.props.items, callback: this.props.callback, inline: this.props.inline, selectedValueOrIndex: this.props.defaultValue, connected: false, classes: this.props.classes });
    }
}
exports.Radio = Radio;
class RadioConnected extends React.Component {
    render() {
        return React.createElement(RadioBase, { title: this.props.title, items: this.props.items, callback: this.props.callback, inline: this.props.inline, selectedValueOrIndex: this.props.selectedIndex, connected: true, classes: this.props.classes });
    }
}
RadioConnected.defaultProps = {
    inline: false,
    classes: []
};
exports.RadioConnected = RadioConnected;
//# sourceMappingURL=Radio.js.map