/**
 * Created by glenn on 6/12/2017.
 */
import React = require("react");
import 'jquery-ui';
export interface iRadioItemOpt {
    index?: number;
}
export interface iRadioItem extends iRadioItemOpt {
    groupId: string;
    text: string;
    checked: boolean;
    inline: boolean;
    change: (s: string) => any;
    connected?: boolean;
}
export declare class Radio extends React.Component<{
    title: string;
    items: string[];
    callback: (val: string) => any;
    defaultValue: string;
    inline?: boolean;
    classes?: string[];
}, {}> {
    render(): JSX.Element;
}
interface iRadioConnectedOpt {
    inline: boolean;
    classes: string[];
}
interface iRadioConnected extends iRadioConnectedOpt {
    title: string;
    items: string[];
    callback: (val: string) => any;
    selectedIndex: number;
}
export declare class RadioConnected extends React.Component<iRadioConnected, {}> {
    static defaultProps: {
        inline: boolean;
        classes: any[];
    };
    render(): JSX.Element;
}
export {};
