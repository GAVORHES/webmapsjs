/**
 * Created by glenn on 7/6/2017.
 */
import React = require("react");
export interface iSliderOpt {
    steps: number;
    animate: boolean;
    defaultAnimationInterval: number;
    value: number;
}
export interface iSlider extends iSliderOpt {
    change: (d: number) => any;
}
export declare class Slider extends React.Component<iSlider, {}> {
    static defaultProps: {
        steps: any;
        animate: boolean;
        defaultAnimationInterval: any;
        value: any;
    };
    private uid;
    private startUid;
    private stopUid;
    private previousUid;
    private nextUid;
    private intervalUid;
    private el;
    private previousButton;
    private nextButton;
    private startButton;
    private stopButton;
    private intervalSelect;
    private interval;
    private running;
    private minVal;
    private maxVal;
    private step;
    constructor(props: iSlider, context: Object);
    componentDidMount(): void;
    updateRunning(): void;
    startAnimate(): void;
    stopAnimate(): void;
    restartAnimate(): void;
    increment(v: number): void;
    render(): JSX.Element;
}
