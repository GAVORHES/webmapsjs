"use strict";
/**
 * Created by glenn on 6/12/2017.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const LayerBaseVectorGeoJson_1 = require("../layers/LayerBaseVectorGeoJson");
const projections_1 = require("../olHelpers/projections");
const makeGuid_1 = require("../util/makeGuid");
const get_map_1 = require("./helpers/get_map");
const Draw_1 = require("ol/interaction/Draw");
const Style_1 = require("ol/style/Style");
const Stroke_1 = require("ol/style/Stroke");
const Fill_1 = require("ol/style/Fill");
const GeometryType_1 = require("ol/geom/GeometryType");
class SelectArea extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.selectId = makeGuid_1.default();
        this.cancelId = makeGuid_1.default();
        this.callback = this.props.callback;
        this.areaOverlay = new LayerBaseVectorGeoJson_1.default('', {
            style: new Style_1.default({
                fill: new Fill_1.default({
                    color: 'rgba(255, 0, 237, 0.1)'
                }),
                stroke: new Stroke_1.default({
                    color: 'rgb(255, 0, 237)',
                    width: 2
                })
            }),
            transform: { dataProjection: projections_1.proj4326, featureProjection: projections_1.proj3857 }
        });
        this.draw = new Draw_1.default({
            source: this.areaOverlay.source,
            type: GeometryType_1.default.POLYGON
        });
        // this.draw.on('drawend', (evt: {feature: {getGeometry: () => Polygon}}) => {
        this.draw.on('drawend', (evt) => {
            this.selectButton.style.display = '';
            this.cancelButton.style.display = 'none';
            let geom = evt['feature'].getGeometry();
            let geomClone = geom.clone();
            geomClone.transform('EPSG:3857', 'EPSG:4326');
            setTimeout(() => {
                this.map.removeInteraction(this.draw);
            }, 100);
            let outCoords = [];
            let ccc = geomClone.getCoordinates()[0];
            for (let cc of ccc) {
                outCoords.push([Math.round(cc[0] * 1000000) / 1000000, Math.round(cc[1] * 1000000) / 1000000]);
            }
            this.callback(outCoords);
        });
    }
    componentDidMount() {
        this.selectButton = document.getElementById(this.selectId);
        this.cancelButton = document.getElementById(this.cancelId);
        get_map_1.default(this.props.map, this.areaOverlay.olLayer).then((m) => { this.map = m; });
    }
    setArea() {
        if (!this.map) {
            return;
        }
        this.selectButton.style.display = 'none';
        this.cancelButton.style.display = '';
        this.areaOverlay.source.clear();
        this.map.addInteraction(this.draw);
        this.callback(null);
    }
    cancel() {
        if (!this.map) {
            return;
        }
        this.selectButton.style.display = '';
        this.cancelButton.style.display = 'none';
        this.areaOverlay.source.clear();
        this.map.removeInteraction(this.draw);
        this.callback(null);
    }
    render() {
        return React.createElement("div", { className: "ol-select-area", style: { margin: '10px' } },
            React.createElement("button", { id: this.selectId, onClick: () => {
                    this.setArea();
                } }, "Select Area"),
            React.createElement("button", { id: this.cancelId, onClick: () => {
                    this.cancel();
                }, style: { display: 'none' } }, "Cancel"));
    }
}
exports.SelectArea = SelectArea;
//# sourceMappingURL=SelectArea.js.map