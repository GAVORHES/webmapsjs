"use strict";
/**
 * Created by gavorhes on 5/13/2016.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const makeGuid_1 = require("../util/makeGuid");
const provide_1 = require("../util/provide");
const nm = provide_1.default('domUtil');
class SelectBoxBase {
    /**
     *
     * @param {jQuery} parent - parent container
     * @param {string} labelContent
     * @param {contentGenerator} [contentGen=undefined]
     */
    constructor(parent, labelContent, contentGen) {
        let guidTop = makeGuid_1.default();
        let guid = makeGuid_1.default();
        let htmlString = `<div id="${guidTop}">`;
        htmlString += `<label for="${guid}">${labelContent}</label>`;
        if (contentGen) {
            htmlString += contentGen(guid);
        }
        else {
            htmlString += `<select id="${guid}"></select>`;
        }
        htmlString += '</div>';
        parent.append(htmlString);
        this._$container = parent.find('#' + guidTop);
        this.$label = this._$container.find('label');
        /**
         *
         * @type {Array<selectChangeCallback>}
         * @private
         */
        this._changeListeners = [];
        this._box = parent.find(`#${guid}`);
        if (!this._box) {
            throw 'the select box was not found';
        }
        this._box.change(() => {
            this.changed();
        });
    }
    /**
     *
     * @returns {jQuery}
     */
    get box() {
        return this._box;
    }
    changed() {
        let v = this._box.val();
        for (let f of this._changeListeners) {
            f(v);
        }
    }
    /**
     *
     * @param {selectChangeCallback} func
     */
    addChangeListener(func) {
        this._changeListeners.push(func);
    }
    get selectedValue() {
        let theVal = this.box.val();
        if (theVal == null || typeof theVal == 'undefined') {
            return null;
        }
        else if (isNaN(theVal)) {
            return theVal;
        }
        else {
            if (theVal.indexOf('.') > -1) {
                return parseFloat(theVal);
            }
            else {
                return parseInt(theVal);
            }
        }
    }
    get selectedIndex() {
        return this._box[0].selectedIndex;
    }
    /**
     *
     * @param {string|number} v
     */
    set selectedValue(v) {
        this.box.val(v);
    }
    get selectedText() {
        return this.box.find('option:selected').text();
    }
}
exports.SelectBoxBase = SelectBoxBase;
nm.SelectBoxBase = SelectBoxBase;
exports.default = SelectBoxBase;
//# sourceMappingURL=SelectBoxBase.js.map