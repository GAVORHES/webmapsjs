"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by gavorhes on 12/4/2015.
 */
const RealEarthAnimate_1 = require("./RealEarthAnimate");
const provide_1 = require("../util/provide");
const nm = provide_1.default('mixin');
/**
 * Animate real earth tile
 * @augments RealEarthAnimate
 */
class RealEarthAnimateTile extends RealEarthAnimate_1.default {
    constructor(layer, loadCallback) {
        super(layer, loadCallback);
        this._source = layer.source;
        this._olLayer = layer.olLayer;
    }
    timeInit() {
        super.timeInit();
        this._sourceUrls = [];
    }
    _loadDates(inString) {
        let rawDte = super._loadDates(inString);
        let dteProductUrl = `http://realearth.ssec.wisc.edu/api/image?products=${this._products}_${rawDte}&x={x}&y={y}&z={z}`;
        this._sourceUrls.push(dteProductUrl);
        return '';
    }
    /**
     * @protected
     */
    _loadLatest() {
        if (super._loadLatest()) {
            this._source.setUrl(this._sourceUrls[this._sourceUrls.length - 1]);
        }
        return true;
    }
    setLayerTime(theTime) {
        if (super.setLayerTime(theTime)) {
            if (this._olLayer.getZIndex() < 0) {
                this._olLayer.setZIndex(0);
            }
            this._source.setUrl(this._sourceUrls[this._currentIndex]);
        }
        else {
            this._olLayer.setZIndex(-1);
        }
        return true;
    }
}
nm.RealEarthAnimateTile = RealEarthAnimateTile;
exports.default = RealEarthAnimateTile;
//# sourceMappingURL=RealEarthAnimateTile.js.map