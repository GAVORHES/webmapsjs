"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const actions = require("./reacttst_actions");
const Redux = require("redux");
function oneDate(state = new Date(), action) {
    if (action.type == actions.SET_ONE_DATE) {
        return action.d;
    }
    else {
        return state;
    }
}
let start = new Date('12/31/2017');
let end = new Date('12/31/2017');
start.setDate(start.getDate() - 10);
function twoDates(state = { start: start, end: end, version: 2 }, action) {
    if (action.type == actions.SET_TWO_DATES) {
        return {
            start: action.start,
            end: action.end,
            version: action.version
        };
    }
    else {
        return state;
    }
}
exports.theStore = Redux.createStore(Redux.combineReducers({ oneDate, twoDates }));
function getState() {
    return exports.theStore.getState();
}
exports.getState = getState;
exports.default = exports.theStore;
//# sourceMappingURL=reacttst_store.js.map