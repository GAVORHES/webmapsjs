"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_redux_1 = require("react-redux");
const DatePick_1 = require("../../reactComponents/DatePick");
const DateRange_1 = require("../../reactComponents/DateRange");
const actions = require("./reacttst_actions");
exports.DatePickConnected = react_redux_1.connect((state) => {
    return {
        label: 'Date Picker Connected',
        change: (v) => {
            console.log(v);
        },
        val: state.oneDate
    };
}, (dispatch) => {
    return {
        change: (v) => {
            dispatch({ type: actions.SET_ONE_DATE, d: v });
        }
    };
})(DatePick_1.DatePick);
exports.DateRangeConnected = react_redux_1.connect((state) => {
    return {
        maxRange: 10,
        start: state.twoDates.start,
        end: state.twoDates.end,
        initialEnd: state.twoDates.end,
        npmrds: true
    };
}, (dispatch) => {
    return {
        callback: (s, e, version) => {
            dispatch({ type: actions.SET_TWO_DATES, start: s, end: e, version: version });
        }
    };
})(DateRange_1.DateRange);
//# sourceMappingURL=reacttst_connect.js.map