import { DatePick } from '../../reactComponents/DatePick';
import { DateRange } from '../../reactComponents/DateRange';
export declare const DatePickConnected: import("react-redux").ConnectedComponentClass<typeof DatePick, Pick<import("../../reactComponents/DatePick").iDatePick, "id" | "initialDate">>;
export declare const DateRangeConnected: import("react-redux").ConnectedComponentClass<typeof DateRange, Pick<import("../../reactComponents/DateRange").iDateRange, "minRange" | "maxDate" | "minDate">>;
