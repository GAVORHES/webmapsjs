"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by gavorhes on 9/23/2016.
 */
const quickMap_1 = require("../../olHelpers/quickMap");
const LayerEsriMapServer_1 = require("../../layers/LayerEsriMapServer");
const LayerLegend_1 = require("../../collections/LayerLegend");
let map = quickMap_1.quickMap();
let wisDotRegions = new LayerEsriMapServer_1.LayerEsriMapServer('https://transportal.cee.wisc.edu/applications/arcgis2/rest/services/MetaManager/Metamanager_regions/MapServer');
let sixYearPlan = new LayerEsriMapServer_1.LayerEsriMapServer('https://transportal.cee.wisc.edu/applications/arcgis2/rest/services/MetaManager/SixYearPlan/MapServer', { name: 'Six Year Plan', legendCollapse: true });
map.addLayer(wisDotRegions.olLayer);
map.addLayer(sixYearPlan.olLayer);
let layerArray = [
    wisDotRegions,
    sixYearPlan
    // tipConfig.tipSegmentLayer,
    // tipConfig.metamanagerSegments,
    // {
    //     groupName: 'ITS Inventory Layers',
    //     collapse: true,
    //     addCheck: false,
    //     items: tipConfig.itsLayerCollection.layers
    // }
];
let legend = new LayerLegend_1.default(layerArray, 'legend-container', {});
//# sourceMappingURL=simple_map.js.map