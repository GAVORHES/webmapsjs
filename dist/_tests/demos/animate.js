"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const quickMap_1 = require("../../olHelpers/quickMap");
const LayerRealEarthTile_1 = require("../../layers/LayerRealEarthTile");
const media_control_1 = require("../../domUtil/media-control");
const $ = require("jquery");
const LayerBaseVectorEsri_1 = require("../../layers/LayerBaseVectorEsri");
const LayerEsriMapServer_1 = require("../../layers/LayerEsriMapServer");
let nexrhresStatic = new LayerRealEarthTile_1.default({
    products: 'nexrhres',
    id: 'nexrhres-static',
    opacity: 0.6,
    animate: true,
    name: 'Hybrid Reflectivity',
    // maxZoom: 10,
    timeLoadCallback: function (f) {
        console.log(f);
    }
});
let d = new Date();
let endTime = d.getTime();
d.setHours(d.getHours() - 4);
let startTime = d.getTime();
let rangeStep = Math.round((endTime - startTime) / 8);
let media = new media_control_1.MediaControl($('#control'), (v) => {
    nexrhresStatic.setLayerTime(v);
}, {
    min: startTime,
    max: endTime,
    val: endTime,
    step: rangeStep,
    playInterval: 750,
    showAsDate: true
});
let map = quickMap_1.quickMap();
map.addLayer(nexrhresStatic.olLayer);
let coordinationLayer = new LayerBaseVectorEsri_1.LayerBaseVectorEsri('https://transportal.cee.wisc.edu/applications/arcgis2/rest/services/GLRTOC/GlrtocCoordination/MapServer/0', {
    visible: true,
    autoLoad: true,
    name: 'Coordination',
    useEsriStyle: true
});
map.addLayer(coordinationLayer.olLayer);
let oakRidgeLayers = [
    ['Cameras', 'cameras33'],
    ['HAR', 'HAR33'],
    ['DMS', 'MessageSigns33'],
    //['State Summary', 'statesummary'],
    ['Traffic Control', 'TrafficControl33'],
    ['Traffic Detection', 'TrafficDetectionMulti'],
    ['Weather', 'Weather33']
];
for (let i = 0; i < oakRidgeLayers.length; i++) {
    let oakRidgeLayer = new LayerEsriMapServer_1.LayerEsriMapServer(`http://itsdpro.ornl.gov/arcgis/rest/services/ITSPublic/${oakRidgeLayers[i][1]}/MapServer`, {
        id: oakRidgeLayers[i][1],
        name: oakRidgeLayers[i][0],
        visible: true,
        minZoom: 7,
        zIndex: 20,
        addPopup: true,
        legendCollapse: true
    });
    map.addLayer(oakRidgeLayer.olLayer);
}
//# sourceMappingURL=animate.js.map