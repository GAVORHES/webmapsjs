"use strict";
/**
 * Created by gavorhes on 12/18/2015.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const ItsLayerCollection_1 = require("../../collections/ItsLayerCollection");
const LayerLegend_1 = require("../../collections/LayerLegend");
const quickMap_1 = require("../../olHelpers/quickMap");
let map = quickMap_1.default({ addGeocode: true });
window['map'] = map;
let itsLayerCollection = new ItsLayerCollection_1.default(map);
let layerArray = [
    {
        groupName: 'ITS Inventory Layers',
        collapse: false,
        addCheck: true,
        items: itsLayerCollection.layers
    }
];
let legend = new LayerLegend_1.default(layerArray, 'legend-container', {});
console.log('it works');
//# sourceMappingURL=itsMap.js.map